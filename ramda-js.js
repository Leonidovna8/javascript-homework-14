const theme = window.localStorage.getItem("theme");
const container = document.getElementById("container");

if (theme === "green") {
  container.classList.add(theme);
}

const button = document.getElementById("button");
button.addEventListener("click", (event) => {
  event.preventDefault();
  container.classList.toggle("green");

  const currentTheme = container.classList.contains("green") ? "green" : "light";
  window.localStorage.setItem("theme", currentTheme);
});

if (!theme) {
  window.localStorage.setItem("theme", "light");
} else if (theme === "green") {
  container.classList.add(theme);
}